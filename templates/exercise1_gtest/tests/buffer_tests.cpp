#include "Buffer.h"
#include <gtest/gtest.h>

TEST(setup_test_case, testWillPass)
{
    ASSERT_EQ(42, 42);
}

TEST(setup_test_case, testWillFail)
{
    ASSERT_EQ(42, 0);
}
