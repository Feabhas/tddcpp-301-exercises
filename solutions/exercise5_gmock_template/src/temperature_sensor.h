/** @file temperature_sensor.h
    \brief API for the abstracted Temperature Sensor module.
*/
#ifndef _TEMPERATURE_SENSOR_H
#define _TEMPERATURE_SENSOR_H

#include <cstdio>
#include <concepts>

template <typename T>
concept CDisplay = requires(T a, const char* str) {
    { a.display(str) } -> std::same_as<int>;
};

template <CDisplay tDisplay, typename tSensor> 
class Temperature_sensor {
public:
  enum class status { ok, sensor_failure, display_failure };
  
  Temperature_sensor(tDisplay &tDisplay_, tSensor &sensor_)
      : display{&tDisplay_}, sensor{&sensor_} {}
  ~Temperature_sensor() = default;

  /*! 
  \fn void Temperture_sensor::initialize()
  \brief Perform initialization of the sensor and display
  \return whether the initialization was successful.
  */
  status initialize() {
    auto count = display->display(sensor->family_code().c_str());
    if (count == -1) {
      return status::display_failure;
    }

    count = display->display(sensor->serial_number().c_str());

    if (count == -1) {
      // display display error
      return status::display_failure;
    }
    return status::ok;
  } 
  
  /*! 
  \fn void Temperture_sensor::run()
  \brief Perform the reading of the raw temperature sensor and displays the value 
  \return whether the reading and conversion of the temperature was successful.
     */
  status run() {
    const auto deg_C = sensor->lastest_reading();
    if ((deg_C < -55.0f) || (deg_C > 125.0f)) {
      // Out of range error
      return status::sensor_failure;
    }

    char buff[20] = {};
    sprintf(buff, "%02.2fC", deg_C);

    const auto count = display->display(buff);

    if (count == -1) {
      return status::display_failure;
    }
    return status::ok;
  }

private:
  tDisplay *display{};
  tSensor *sensor{};
};



#endif // _TEMPERATURE_SENSOR_H
